module.exports = {
    title: 'Kevin - Blog',
    description: 'Hello',
    themeConfig: {
        nav: [
            { text: 'Home', link: '/'},
            { text: 'Guide', link: '/guide/'},
            { text: 'About', link: 'https://www.kevinbrendo.tk'},
        ],
        sidebar: {
            '/guide/': [
                '',
                'firstpost'
            ]
        }
    }
}